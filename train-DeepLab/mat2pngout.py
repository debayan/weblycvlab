#!/usr/bin/env python
# Martin Kersner, m.kersner@gmail.com
# Modified by debayan for final mat 2 png conversion
# 2016/03/17

from __future__ import print_function
import os
import numpy as np
import sys
import glob
from PIL import Image as PILImage

from utils import convert_segmentation_mat2numpy

palette = { 0 : (  0,   0,   0) ,
             1 : (128,   0,   0) ,
             2 : (  0, 128,   0),
             3 : (128, 128,   0),
             4 : (  0,   0, 128),
            5 : (128,   0, 128),
             6 : (  0, 128, 128),
             7 : (128, 128, 128),
             8 : ( 64,   0,   0),
             9 : (192,   0,   0),
            10: ( 64, 128,   0),
             11: (192, 128,   0),
             12: ( 64,   0, 128),
            13: (192,   0, 128),
             14: ( 64, 128, 128),
             15: (192, 128, 128),
             16: (  0,  64,   0),
             17: (128,  64,   0),
             18:  (  0, 192,   0),
             19: (128, 192,   0),
             20: (  0,  64, 128)}

def main():
  input_path, output_path = process_arguments(sys.argv) 

  if os.path.isdir(input_path) and os.path.isdir(output_path):
    mat_files = glob.glob(os.path.join(input_path, '*.mat'))
    convert_mat2png(mat_files, output_path)
  else:
    help('Input or output path does not exist!\n')

def process_arguments(argv):
  num_args = len(argv)

  input_path  = None
  output_path = None 

  if num_args == 3:
    input_path  = argv[1]
    output_path = argv[2]
  else:
    help()

  return input_path, output_path

def convert_mat2png(mat_files, output_path):
  if not mat_files:
    help('Input directory does not contain any Matlab files!\n')

  for mat in mat_files:
    numpy_img = convert_segmentation_mat2numpy(mat)
    newimg = np.zeros((numpy_img.shape[0],numpy_img.shape[1],3), dtype="uint8")
    for i in np.ndindex(numpy_img.shape[:2]):
        newimg[i] = palette[numpy_img[i]]
    pil_img = PILImage.fromarray(newimg)
    pil_img.save(os.path.join(output_path, modify_image_name(mat, 'png')))

# Extract name of image from given path, replace its extension with specified one
# and return new name only, not path.
def modify_image_name(path, ext):
  return os.path.basename(path).split('.')[0] + '.' + ext

def help(msg=''):
  print(msg +
        'Usage: python mat2png.py INPUT_PATH OUTPUT_PATH\n'
        'INPUT_PATH denotes path containing Matlab files for conversion.\n'
        'OUTPUT_PATH denotes path where converted Png files ar going to be saved.'
        , file=sys.stderr)

  exit()

if __name__ == '__main__':
  main()
