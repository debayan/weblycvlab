#!/usr/bin/env sh

# Modified version from https://bitbucket.org/deeplab/deeplab-public
# Martin Kersner, m.kersner@gmail.com
# 2016/04/05

CAFFE_BIN=code/build/tools/caffe
EXP=exper/pastely
DATA_ROOT=exper/pastely/data
GPU_ID=0

# Specify number of classes
NUM_LABELS=21      # all classes
#NUM_LABELS=4         # 3 classes + 1 background

LIST_SUFFIX=         # all classes
#LIST_SUFFIX=_subset   # only for limited number of classes

# Specify which dataset use for training
TRAIN_SET_SUFFIX=    # original PASCAL VOC 2012 dataset
#TRAIN_SET_SUFFIX=_aug # augmented PASCAL VOC dataset

# Specify which model to train
NET_ID=pastely

# Run
RUN_TRAIN=1
RUN_TEST=0
RUN_TRAIN2=0
RUN_TEST2=0

# Create directories ###########################################################
CONFIG_DIR=${EXP}/config/${NET_ID}
MODEL_DIR=${EXP}/model/${NET_ID}
LOG_DIR=${EXP}/log/${NET_ID}

for DIR in $CONFIG_DIR $MODEL_DIR $LOG_DIR; do 
  mkdir -p ${DIR}
done

export GLOG_log_dir=${LOG_DIR}

# Training #1 (on train_aug) ###################################################
if [ ${RUN_TRAIN} -eq 1 ]; then
  LIST_DIR=${EXP}/list${LIST_SUFFIX}
  TRAIN_SET=train${TRAIN_SET_SUFFIX}

  MODEL=${EXP}/model/${NET_ID}/vgg16_20M.caffemodel
  #MODEL=finalmodels/wc4kiterstage1.caffemodel

  echo "Training net ${EXP}/${NET_ID}"
  for pname in train solver; do
	  sed "$(eval echo $(cat sub.sed))" \
	    ${CONFIG_DIR}/${pname}.prototxt > ${CONFIG_DIR}/${pname}_${TRAIN_SET}.prototxt
  done

  CMD="${CAFFE_BIN} train \
    --solver=${CONFIG_DIR}/solver_${TRAIN_SET}.prototxt \
    --gpu=${GPU_ID}"

	if [ -f ${MODEL} ]; then
	    CMD="${CMD} --weights=${MODEL}"
	fi

	echo Running ${CMD} && ${CMD}
fi
