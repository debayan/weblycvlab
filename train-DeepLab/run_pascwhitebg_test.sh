#!/usr/bin/env sh

# Modified version from https://bitbucket.org/deeplab/deeplab-public
# Martin Kersner, m.kersner@gmail.com
# 2016/04/05

CAFFE_BIN=code/build/tools/caffe
EXP=exper/pascwhitebg
DATA_ROOT=exper/pascwhitebg/data
GPU_ID=0

# Specify number of classes
NUM_LABELS=21      # all classes
#NUM_LABELS=4         # 3 classes + 1 background

LIST_SUFFIX=         # all classes
#LIST_SUFFIX=_subset   # only for limited number of classes

# Specify which dataset use for training
TRAIN_SET_SUFFIX=    # original PASCAL VOC 2012 dataset
#TRAIN_SET_SUFFIX=_aug # augmented PASCAL VOC dataset

# Specify which model to train
NET_ID=pascwhitebg

# Run
RUN_TRAIN=0
RUN_TEST=1
RUN_TRAIN2=0
RUN_TEST2=0

# Create directories ###########################################################
CONFIG_DIR=${EXP}/config/${NET_ID}
MODEL_DIR=${EXP}/model/${NET_ID}
LOG_DIR=${EXP}/log/${NET_ID}

for DIR in $CONFIG_DIR $MODEL_DIR $LOG_DIR; do 
  mkdir -p ${DIR}
done

export GLOG_log_dir=${LOG_DIR}


# Test #1 specification (on val or test) #######################################
if [ ${RUN_TEST} -eq 1 ]; then
  for TEST_SET in val; do
	  TEST_ITER=`cat exper/webly/list${LIST_SUFFIX}/${TEST_SET}.txt | wc -l`
	  #MODEL=${EXP}/model/${NET_ID}/test.caffemodel
          MODEL=finalmodels/pasc4kstage0.caffemodel

	  if [ ! -f ${MODEL} ]; then
	      MODEL=`ls -t ${EXP}/model/${NET_ID}/train_iter_*.caffemodel | head -n 1`
	  fi
	  
	  echo "Testing net ${EXP}/${NET_ID}"
	  FEATURE_DIR=${EXP}/features/${NET_ID}
	  mkdir -p ${FEATURE_DIR}/${TEST_SET}/fc8
	  mkdir -p ${FEATURE_DIR}/${TEST_SET}/crf

	  sed "$(eval echo $(cat sub.sed))" \
	      ${CONFIG_DIR}/test.prototxt > ${CONFIG_DIR}/test_${TEST_SET}.prototxt

	  CMD="${CAFFE_BIN} test \
      --model=${CONFIG_DIR}/test_${TEST_SET}.prototxt \
      --weights=${MODEL} \
      --gpu=${GPU_ID} \
      --iterations=1449"

	  echo Running ${CMD} && ${CMD}
  done
fi
