#!/usr/bin/python


# -*- coding: utf-8 -*-

import http.client, urllib.parse, json, sys, time, urllib.request, os

# **********************************************
# *** Update or verify the following values. ***
# **********************************************

# Replace the subscriptionKey string value with your valid subscription key.
subscriptionKey = "6f891ea2ebe74f718ecd98f7ca0bdee1"

# Verify the endpoint URI.  At this writing, only one endpoint is used for Bing
# search APIs.  In the future, regional endpoints may be available.  If you
# encounter unexpected authorization errors, double-check this value against
# the endpoint for your Bing Web search instance in your Azure dashboard.
host = "api.cognitive.microsoft.com"
path = "/bing/v7.0/images/search"


pascalclasses = ['person','bird','cat','cow','dog','horse','sheep','aeroplane','bicycle','boat','bus','car','motorbike','train','bottle','chair','dining table','potted plant','sofa','tv monitor']

def BingWebSearch(search, offset):
    "Performs a Bing Web search and returns the results."

    headers = {'Ocp-Apim-Subscription-Key': subscriptionKey}
    conn = http.client.HTTPSConnection(host)
    query = urllib.parse.quote(search)
    conn.request("GET", path + "?q=" + query+'&count=2000&offset=%s'%offset, headers=headers)
    response = conn.getresponse()
    headers = [k + ": " + v for (k, v) in response.getheaders()
                   if k.startswith("BingAPIs-") or k.startswith("X-MSEdge-")]
    return headers, response.read().decode("utf8")

for term in pascalclasses:
    if not os.path.exists('images/'+term):
        os.mkdir('images/'+term)
    offset = 0
    count = 0
    while 1:
        headers, result = BingWebSearch(term+' white background', offset)
        d = json.loads(result)
        for item in d['value']:
            print(item['contentUrl'])
            try:
                urllib.request.urlretrieve(item['contentUrl'], 'images/'+term+'/'+str(count)+'.jpg')
            except Exception:
                print('exception')
            count += 1
        prevoffset = offset
        offset = d['nextOffset']
        if prevoffset == offset:
            break
