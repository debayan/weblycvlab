#!/usr/bin/python

import os,sys

f1 = open(sys.argv[1])
f2 = open(sys.argv[2])

for linew,lines in zip(f1.readlines(),f2.readlines()):
    linew = linew.strip()
    lines = lines.strip()
    outpath = linew.replace('../finalimages/resizedWhiteBg100/','crfbinary/')
    ret = os.system("python crfsaliency.py %s %s %s"%(linew, lines, outpath))
    print linew,lines,outpath
