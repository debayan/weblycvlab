#!/usr/bin/python

import sys,cv2,os
import numpy as np
f = open(sys.argv[1])

labels = ['background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor', 'void']

colors = [[0,0,0], [128,0,0], [  0, 128 ,0], [128, 128,   0], [  0,   0, 128],[128,0,128], [  0, 128, 128], [128, 128, 128], [64,  0,  0], [192,   0,   0], [ 64, 128,   0], [192, 128,   0], [ 64,   0, 128], [192, 0, 128], [ 64, 128, 128], [192, 128, 128], [ 0, 64,  0], [128,  64,   0], [  0, 192,   0], [128, 192,   0], [0, 64, 128]]

for line in f.readlines():
    line = line.strip()
    clas = line.split('/')[3]
    insertcolor = colors[labels.index(clas)]
    im = cv2.imread(line,1)
    im[np.where((im != [0,0,0]).all(axis = 2))] = [insertcolor[2],insertcolor[1],insertcolor[0]]
    newpath = line.replace('../finalimages/crfbinary/','crfcolor/')
    print newpath
    cv2.imwrite(newpath,im)
    #sys.exit(1)
    #cv2.imshow('yo',im)
    #cv2.waitKey(0)
   
    
