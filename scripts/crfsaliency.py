import sys
import numpy as np
import pydensecrf.densecrf as dcrf
import cv2
from pydensecrf.utils import unary_from_labels, create_pairwise_bilateral, create_pairwise_gaussian, unary_from_softmax
import heapq

im1 = cv2.imread(sys.argv[1]) #color image
im2 = cv2.imread(sys.argv[2], 0) #saliency map grayscale
im2 = im2/float(np.max(im2))
h,w = im2.shape

probs = np.tile(im2[np.newaxis,:,:],(2,1,1))
probs[1,:,:] = 1 - probs[0,:,:]


U = unary_from_softmax(probs)  # note: num classes is first dim
d = dcrf.DenseCRF2D(w, h, 2)
d.setUnaryEnergy(U)
d.addPairwiseBilateral(sxy=(40,40), srgb=(13,13,13), rgbim=im1, compat=10, kernel=dcrf.DIAG_KERNEL, normalization=dcrf.NORMALIZE_SYMMETRIC)

Q = d.inference(5)

mapp = np.argmax(Q, axis=0).reshape((im2.shape[0],im2.shape[1]))
mapp = 1-mapp
mapp = mapp*255.0

#cv2.imshow('ha',mapp)
#cv2.waitKey(0)
cv2.imwrite(sys.argv[3], mapp)


