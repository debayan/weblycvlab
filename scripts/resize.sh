#!/bin/bash

# absolute path to image folder
FOLDER="./"

# max height
WIDTH=340

# max width
HEIGHT=340

#resize to either height or width, keeps proportions using imagemagick
images="$(find $FOLDER -iname '*.jpg' -o -iname '*.png')"
for image in $images; do
    echo $image
    convert "$image" -resize 340x340 "$image"
done
