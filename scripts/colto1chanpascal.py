#!/usr/bin/python

import sys,cv2,os
import numpy as np
import multiprocessing

labels = ['background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'chair', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']
palette = {  ( 0,   0,   0) : 0 ,  #RGB, not BGR
             (128,   0,   0) : 1 ,
             (  0, 128,   0) : 2 ,
             (128, 128,   0) : 3 ,
             (  0,   0, 128) : 4 ,
             (128,   0, 128) : 5 ,
             (  0, 128, 128) : 6 ,
             (128, 128, 128) : 7 ,
             ( 64,   0,   0) : 8 ,
             (192,   0,   0) : 9 ,
             ( 64, 128,   0) : 10,
             (192, 128,   0) : 11,
             ( 64,   0, 128) : 12,
             (192,   0, 128) : 13,
             ( 64, 128, 128) : 14,
             (192, 128, 128) : 15,
             (  0,  64,   0) : 16,
             (128,  64,   0) : 17,
             (  0, 192,   0) : 18,
             (128, 192,   0) : 19,
             (  0,  64, 128) : 20 }

def mp_worker((seginid)):
    segout1d = sys.argv[3]+seginid+'.png'
    inpath = sys.argv[2]+seginid+'.png'
    im = cv2.imread(inpath,1)
    newim = np.zeros([im.shape[0],im.shape[1],1],dtype=np.uint8)
    for i in np.ndindex(im.shape[:2]):
        if (im[i][2],im[i][1],im[i][0]) not in palette:
            newim[i] = 0
        else:
            newim[i] = palette[(im[i][2],im[i][1],im[i][0])]
    cv2.imwrite(segout1d, newim)

f = open(sys.argv[1])
infiles = []
for line in f.readlines():
    idd = line.strip()
    infiles.append(idd)
f.close()
print infiles
p = multiprocessing.Pool(3)
p.map(mp_worker, infiles)


