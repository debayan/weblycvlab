#!/usr/bin/python

import sys,cv2,os
import numpy as np
from random import randint
import random

f = open(sys.argv[1]) #background images
backgroundimages = []
for line in f.readlines():
    line = line.strip()
    im = cv2.imread(line)
    backgroundimages.append(im)
f.close()

print "Completed loading background images"

f = open(sys.argv[2]) #color segmentaions ids
for line in f.readlines():
    idd = line.strip()
    seginpath = 'SegmentationClass/'+idd+'.png'
    realinpath = 'RealImages/'+idd+'.jpg'
    print seginpath,realinpath
    segim = cv2.imread(seginpath)
    realim = cv2.imread(realinpath)
    newmask = np.zeros([segim.shape[0],segim.shape[1],1],dtype=np.uint8)
    segout = np.zeros([segim.shape[0],segim.shape[1],3],dtype=np.uint8)
    newmask.fill(1)
    newmask[np.where((segim==0).all(axis=2))] = 0
    for index,bgimg in enumerate(random.sample(backgroundimages,10)):
        realim_resized = cv2.resize(realim, (bgimg.shape[1],bgimg.shape[0]))
        newmask_resized = cv2.resize(newmask, (bgimg.shape[1],bgimg.shape[0]))
        segim_resized = cv2.resize(segim,(bgimg.shape[1],bgimg.shape[0]))
        segout_resized = cv2.resize(segout,(bgimg.shape[1],bgimg.shape[0]))
        out=bgimg.copy()
        hshift = randint(-50,50)
        wshift = randint(-50,50)
        for i in np.ndindex(out.shape[:2]):
            if i[0]+hshift >= out.shape[0] or i[1]+wshift >= out.shape[1]:
                continue
            if newmask_resized[i]==1:
                out[(i[0]+hshift,i[1]+wshift)] = realim_resized[i]
                segout_resized[(i[0]+hshift,i[1]+wshift)] = segim_resized[i]
        cv2.imwrite('combreal/'+idd+'_'+str(index)+'.png', out)
        cv2.imwrite('combsegout/'+idd+'_'+str(index)+'.png', segout_resized)
