\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tabularx}
\usepackage{lipsum}% http://ctan.org/pkg/lipsum

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

\cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

\DeclareMathOperator*{\argmax}{arg\,max}

% Pages are numbered in submission mode, and unnumbered in camera-ready
%\ifcvprfinal\pagestyle{empty}\fi
\setcounter{page}{1}
\begin{document}

%%%%%%%%% TITLE
\title{Webly vs Transparent Pastely : Two approaches of weak supervision for image segmentation}

\author{Debayan Banerjee\\
University of Bonn\\
{\tt\small debayan@uni-bonn.de}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Johann Sawatzky\\
University of Bonn\\
{\tt\small sawatzky@iai.uni-bonn.de}
\and
Juergen Gall\\
University of Bonn\\
{\tt\small gall@iai.uni-bonn.de}
}


\maketitle
%\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
   We take inspiration from the Webly approach towards image segmentation \cite{7532767} and try certain 
   modifications to the original approach and compare them to each other.
   The Webly approach involves downloading white background images from the web and using saliency detection \cite{6619251} to extract foreground
   from these images. Foreground pixels from these images and common background images are used to train several shallow neural networks 
   and a DCNN aided by a CRF to refine and retrain the networks eventually resulting in a weakly supervised image segmentation neural network.
   We identify certain points of failure of Webly and make modifications to mitigate them. We also try to find out how a simpler 
   method of training a single DCNN end-to-end  with different sets of images performs. At the end we devise a new method named Transparent Pastely
   where we download images with transparent backgrounds from the web and paste foreground portions at random locations on common background images 
   generating only one set of training images which are used as training set for a DCNN to outperform Webly.
\end{abstract}

\section{Introduction}

Semantic segmentation, which refers to accurately assigning
semantic labels to the corresponding pixels in an
image, is a challenging task actively studied in computer
vision.  Recent breakthroughs  in semantic
segmentation are mainly due to the fully supervised algorithms
that apply Convolutional Neural Networks (CNNs)
on datasets that contain images and their pixel-wise annotations,
e.g., PASCAL VOC \cite{pascal-voc-2012}  and Microsoft COCO \cite{2014arXiv1405.0312L}. Extending fully
supervised algorithms to more object classes, however, requires
collecting massive amount of pixel-wise annotations,
which is both time-consuming and expensive. Thus, other annotations
that are less precise but faster to collect, such as 
points, scribbles, or bounding boxes, have also been employed
to supervise semantic segmentation. This approach is also known as weakly supervised image segmentation.\\
\\

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.8\linewidth]{images/image3types.png}
\end{center}
   \caption{To supervise semantic segmentation, the Webly approach involves 
   extracting three sets of web images: images with white background
\textbf{Wk}, images that contain common background scenes
\textbf{C}, and realistic images \textbf{Rk}.}
\label{fig:long}
\label{fig:onecol}
\end{figure}

We start by looking at Webly supervised image segmentation \cite{7532767}. This method 
employs downloading 3 sets of images. These three sets of images
include the objects on white background \textbf{Wk}, the common
backgrounds \textbf{C}, and the realistic scenes of the classes \textbf{Rk} as illustrated in Figure 1. They propose
a novel three-stage procedure to progressively train their
semantic segmentation model, as illustrated in Figure 2. In
the first stage, a Shallow Neural Network (SNN) is initially
trained to predict class-specific segmentation masks, using
the hypercolumn features \cite{2014arXiv1411.5752H} from images with white
background and images with common backgrounds. They
then iteratively refine the SNN of each class on a set of realistic
images of that class to generate better segmentation
masks. In the last stage, the SNNs of all classes are assembled
into one Deep Convolutional Neural Network (DCNN)
by training the DCNN with the predicted multi-label segmentation
masks from the SNNs. The DCNN is then tested on the PASCAL
VOC 2012 segmentation benchmark. \\
In all our experiments we decide to use a single DCNN called the DeepLab-LargeFoV \cite{2016arXiv160600915C} instead of the multiple class-sepcific 
SNNs and DCNN that Webly \cite{7532767} uses.
We do this to simplify the process of training and focus our efforts more towards generation of proper training data.
We first download the 3 sets of images that Webly proposes and train and refine a DCNN on these 3 sets. We identify the initial step of
foreground extraction using saliency detection \cite{6619251} as a point of failure leading to poor quality training data.
We then replace the saliency detection based extraction of foreground from white background images  and try to find out how a training set built out of perfectly
segmented foreground images pasted on white background affects the segmentation accuracy. We first do this by using PASCAL VOC 2012 \cite{pascal-voc-2012} training set 
which has perfectly labelled foregrounds. We extract these foregrounds and paste them on white backgrounds. When pasting we note the pixel locations of the paste and use
it to generate perfect training segmentation masks.
In a later experiment we paste these foregrounds directly on common background images at random positions and random scales leading to generation of a good amount of 
synthetic augmented training data. We call this approach Pastely. We find both these methods outperform Webly hence demonstrating that having well segmented foreground 
regions during first stage of training is important and that a larger number of images helps get better accuracy. \\
Since using perfectly labelled PASCAL VOC images for training no longer allows us to call the method weakly supervised, we adopt a new approach in our final experiment.
We download images with transparent background from the web. While the Webly approach downloads images with white background, the process of extraction of 
foreground from the white background images are still not perfect. Hence it is better to look for images with alpha channel (typically PNG images) which already
have background perfectly removed. After we download the images we paste the foregrounds on common background images at random locations at random scales leading
to synthetic augmented training data set. We do no human curation during this process leading to perfectly weakly supervised image segmentation. We call this approach
Transparent-Pastely and it outperforms Webly.\\

In summary our \textbf{main contributions} are:

\begin{itemize}
  \item To show that saliency detection is a weak aspect of Webly approach
  \item To show that the multi-phase training approach of Webly is unnecessary
  \item To show that transparent background images are better than white background images for this task
  \item To show that synthetic training data generation by randomly pasting foreground on common background images is a sound approach
\end{itemize}

\begin{figure*}[t]
\begin{center}
\includegraphics[width=1\linewidth]{images/weblypipeline.png}
\end{center}
   \caption{ The three-stage Webly training pipeline. (a) Stage one: initial training of the SNN using hypercolumn features
from Wk and C. (b) Stage two: iterative refinement of the SNN on realistic images Rk. (c) Stage three: all SNNs are
assembled into one DCNN for end-to-end training and testing.}
\end{figure*}

\section{Related work}

\textbf{Weakly Supervised Semantic Segmentation}: The methods we experiment with mainly belong to the family of weakly supervised segmentation algorithms that require 
only image tag annotation. Here we review the CNN-based approaches as these methods [\cite{psld15},\cite{pc15},\cite{pkm15},\cite{pkd15},\cite{wlcscy15},\cite{kl},
,\cite{sy16},\cite{sasla16},\cite{qlszj16},\cite{wlcjxz16}] provide good 
segmentation quality on the challenging PASCAL VOC benchmark. Early works [\cite{psld15}, \cite{pc15}, \cite{pkd15}] extend the MultipleInstance Learning (MIL) [\cite{ath}] framework for weakly supervised
semantic segmentation, where the loss functions are built on the image tags level. They adopt different approaches, e.g., maximum pool \cite{psld15} or Log-Sum-Exp \cite{pc15},
to pool pixel-level probability predictions into image-level losses. No object location information is considered in these frameworks, thus resulting in coarse segmentation masks.
Recent methods [\cite{kl},\cite{sy16},\cite{sasla16},\cite{qlszj16},\cite{wlcjxz16}] investigate how to automatically infer the location of each class without pixelwise annotations. 
\cite{sy16} and \cite{sasla16} both build their location cues by inversing the pre-trained classification network \cite{sz15} on ImageNet \cite{rdskshkkmf15}. The difference is that
\cite{sy16} builds general objectness measure for all classes while \cite{sasla16} focuses on
class-specific saliency maps. Similar location cues are used in \cite{kl} in the form of the seed loss. They further integrate the seed loss with another two losses that encode more 
location information. The bottom up segment proposals \cite{apba14} are used as another approach to obtain location information of each class in \cite{qlszj16},\cite{wlcjxz16}. 
Instead of inferring rough location 
cues for each class using objectness or saliency maps, Webly trains a shallow segmentation network from web images to automatically generate segmentation masks for each class.
Using these segmentation masks, Webly further trains a DCNN that performs semantic segmentation. \\ 
Moreover, significant research in transfer learning from synthetically rendered images to real images has been
published \cite{RosCVPR16}, \cite{2017arXiv170105524P}. Peng et al. \cite{2017arXiv170105524P} have done progressive work in the Object Detection context, understanding
various cues affecting transfer learning from synthetic images. Rajpura et al \cite{2017arXiv170900849R} try to combine 3d models rendered on common background images
to train a CNN and this approach is similar to our Transparent-Pastely approach. \\
\cite{2017arXiv170108261O} use saliency for image segmentation and \cite{2017arXiv170705821C} and \cite{2017arXiv170509052S} are two notable weakly supervised image 
segmentation approaches. \\
\textbf{Webly Supervised Computer Vision}:  Recently, Wei et al. \cite{2015arXiv150903150W} also propose to use web images to 
train CNNs for semantic segmentation. While Webly extracts two sets of images that distinctly separate foreground and background, \cite{2015arXiv150903150W} 
employs Flickr images that may have cluttered  backgrounds. \\ 
\textbf{Fully Supervised Semantic Segmentation}: For completeness sake, we also quickly 
review some fully supervised semantic segmentation methods \cite{lsd15},\cite{cpkm15},\cite{zjrvdht15},\cite{lsr},\cite{nhh15},\cite{bst16} that rely on pixel-wise \
annotations for training. No surprise, they report excellent performance on the 
standard segmentation benchmark. These methods build upon the Fully Convolutional Network (FCN) architecture \cite{lsd15}  to perform end-to-end training. 
Techniques like Conditional Random
Field (CRF) \cite{cpkm15},\cite{zjrvdht15},\cite{lsr}, deconvolution \cite{nhh15} and Boundary Neural Fields (BNF) \cite{bst16} are further combined with the FCN. 
However, since pixel-wise annotations are costly and time consuming
to obtain, the scalability of these methods is limited.

\section{Webly}

Webly \cite{7532767} requires downloading 3 sets of images. These three sets of images
include the objects on white background \textbf{Wk}, the common backgrounds \textbf{C}, and the realistic scenes of the classes \textbf{Rk} as illustrated in Figure 1.
Webly is comprised of 3 stages. (Refer Figure 2) \\

\textbf{Stage 1 - Initial Training:} They denote \( \Omega = \{1, 2, ..., N\} \) as the set of class names
where N is the number of classes. For each class \(k \in \Omega \), a
SNN, whose parameters is denoted as \(\theta_k\), is trained using
the images in \textbf{Wk} and \textbf{C}. Since objects in \textbf{Wk} are surrounded
by white background, the foreground pixels from \textbf{Wk}, denoted as \( W^f_k \)
, thus represent the visual appearance of class k. Correspondingly, the pixels in \textbf{C} represent the
visual appearance of the common backgrounds. They use
hypercolumn features \cite{HariharanABMM11} extracted from the pre-trained
VGG16 network \cite{sz15} to encode the visual appearance. For
each pixel \( x^k_i\), we compute its hypercolumn
feature: \[ h^k_i = H(x^k_i) \]. Here H represents the operation to compute hypercolumn
features. They then use the hypercolumn features from \textbf{C} and \( W^f_k \)
to train the SNN, as shown in Figure 2(a).\\
\(\theta_k\) is needed to minimize the binary crossentropy loss: \[ \min_{\theta_k} \sum_i -(t^k_i log(f(h^k_i,\theta_k)) + (1 - t^k_i) log (1 - f(h^k_i,\theta_k) \]
Here \(f(h^k_i,\theta_k) \) represents the SNN output. The SNN takes
in the hypercolumn feature \(h^k_i\) and outputs the probability of \(x^k_i\) belongs to class k. \[ t^k_i =
    \begin{cases}
      0, & \text{if } x^k_i \in  C \\
      1, & \text{if } x^k_i \in W^f_k
    \end{cases} \]
An equal number of hypercolumn features are randomly extracted
from C and \(W^f_k\) ,forming a balanced set for training the SNN. After initial training, the SNN of class k can
predict the probability of a pixel belonging to class k, effectively outputting a class-specific mask for an image


\textbf{Stage 2 - Refinement:} \(\theta_k\) is initially trained to separate the objects of class
k from the common backgrounds. However, the realistic
background of class k may be different from the common
backgrounds. They thus further iteratively refine \(\theta_k\) on realistic images, as shown in Figure 2(b). They make use of multiple
CRF iterations to improve our SNNs. CRF has been
shown to be helpful for semantic segmentation as it recovers
missing parts and refines the boundaries in the segmentation
masks \cite{2016arXiv160600915C}, \cite{kl}, \cite{sasla16}. . Unlike most methods that apply one
time CRF as post-processing, they apply CRF in each refinement
iteration and learn to update the SNNs according to
the CRF-refined masks. Consequently, the SNNs are forced
to gradually learn to generate more complete segmentation
masks with better boundaries.\\
Assume \(x^k_i\) represents the ith pixel in the collection of
pixels from the realistic image set Rk. \(h^k_i\) and \(y^k_i\)
are its corresponding hypercolumn feature and label (0 for background
and 1 for foreground). They refine \(\theta_k\) by minimizing
the loss:  \[ \min_{\theta_k} \sum_i -(y^k_i log(f(h^k_i,\theta_k)) + (1 - y^k_i) log (1 - f(h^k_i,\theta_k) \]
Given the parameters \(\theta_k\) of the SNN, the labels \(y^k_i\) are
predicted by minimizing the dense CRF energy function: \[ \min_{y^k_i} \sum_i \Phi^k_i(y^k_i) + \sum_{ij} \Phi^k_{ij} (y^k_i,y^k_j)\qquad(1) \]
where the SNN’s output is adopted for the unary term: \[ \Phi^k_i(y^k_i) = -log(y^k_i f(h^k_i,\theta_k) + (1 - y^k_i)(1 - f(h^k_i,\theta_k)))\quad(2) \]
The pairwise term is set to be the standard color and spatial
distance as in \cite{uk11}. An Expectation-Maximization (EM)
algorithm is adopted to iteratively optimize Equation 1 and
Equation 2. In each iteration, they first update the labels of
all pixels \({y^k_i}\) by minimizing Equation 1 using the method
in \cite{uk11}. Then the parameters \(\theta_k\) get updated by minimizing
Equation 1 using back propagation. The new parameters
\(\theta_k\) are used in Equation 2 to obtain the new \({y^k_i}\) as the
beginning of a new iteration.

\textbf{Stage 3 - Assembling of classes:} The SNNs are trained independently for each class. To
perform semantic segmentation for multiple classes, in this
stage they assemble all SNNs into one deep convolution neural
network (DCNN), as shown in Figure 2(c).\\
We use the DCNN architecture proposed in \cite{2016arXiv160600915C} due to
its outstanding performance and good documentation. The
DCNN is a fully convolutional neural network that takes in
an image and directly predicts a multi-label segmentation
mask. They train the DCNN on all realistic images in {Rk}.
Since no pixel-wise human annotations are provided for
training, they use the SNNs to automatically generate multilabel
segmentation masks as supervision. Specifically, if
one image in {Rk} is labeled with tags \(C \subseteq \Omega\), the predicted
label \(y_i\) for the ith pixel is: \[  y_i = \argmax_{k \in \{0\} \cup C} f(h^k_i,\theta_k)\qquad(3)  \]

\[ f(h^0_i,\theta_0) = 1 - \max_{k \in C} f(h^k_i, \theta_k)\qquad(4) \]

Effectively, the combined multi-label segmentation mask is
produced by taking the pixel-wise maximum of the probability maps across all labels, including background (represented
as label 0). The background probability is set as one
minus the maximum foreground probability.\\
After generating the multi-label segmentation masks using
the SNNs, they treat them as the ground truth segmentation
masks and perform end-to-end training of the DCNN.
These multi-label segmentation masks are not human annotations
but automatically generated masks from our SNNs,
which only require image tags during training. Therefore,
the fully supervised DCNN training in \cite{2016arXiv160600915C} is transformed to
weakly supervised in Webly.

\subsection{Webly implementation details}

For the three-stage training, Webly builds Wk by querying
Google with the 20 classes and obtains on average 340 images per class (6807
images in total). The Holiday dataset \cite{Jegou:2008:HEW:1478392.1478419}, which contains
1491 holiday images, serves as the C set since these images
cover some common background scenes, e.g., sky, mountains,
grass. Webly uses the trainaug set of PASCAL VOC 2012
as {Rk} since they are all realistic images of the 20 classes. 
For the refinement with Rk images Webly uses 10,582 images from \cite{HariharanABMM11}.

\subsection{Webly reported results}

\begin{center}
\begin{tabular}{ | m{5em} | m{1cm}| } 
\hline
Method & mIoU \\ 
\hline
\textbf{WebS-i} & 46.4  \\ 
\hline
\textbf{WebS-i1} & 51.6 \\ 
\hline
\textbf{WebS-i2} & 53.4 \\
\hline
\end{tabular}
\end{center}

\textbf{WebS-i} represents the
model that directly combines stage one with stage three,
bypassing the refinement stage. \textbf{WebS-i1} and \textbf{WebS-i2} are
the models trained through all three stages, with one refinement
iteration and two refinement iterations, respectively.\\

\begin{figure}[t]
\begin{center}
\includegraphics[width=1\linewidth]{images/aeroplancesaliencyfail.png}
\end{center}
   \caption{Performance of saliency + CRF on aeroplane images  a) Good quality segmentation b) Saliency map too strong on bright red shade c) Watermark on image affects segmentation
   adversely d) Shade of aeroplane too light for saliency + CRF}
\end{figure}

\section{Experiments}

We now present sequentially a set of experiments each of which modify certain aspects of the Webly approach and measure the change in accuracy.

\subsection{DCNN Only Webly}

\textbf{Aim:} Attempt to simplify the training procedure of Webly.\\

\textbf{Setup:} We continue with image sets \textbf{C} and \textbf{Wk} and choose the DeepLab-LargeFOV \cite{2016arXiv160600915C} network as our DCNN. This network has 
pre-trained VGG16 weights.
This network performs well due to its large field of view and low stride.
We do away with separate SNNs and hypercolumn features. The \textbf{C} and \textbf{Wk} image sets are the training data for our DCNN.
We apply saliency detection on \textbf{W} images which gives us a saliency map with varying levels of grayscale. We then use a dense CRF where the saliency map values
are the unary term, same as in Webly. The CRF thus produces foreground regions . We label white regions as background
and common background images fully as background. We use this set as training data for our DCNN. We then use this trained DCNN to segment realistic images from 
\textbf{R}. The last layer of our network happens to be a CRF which applies 10 iterations of refinement on the images. We use this improved segmentation set to re-train the DCNN.
Hence we only do one step of refinement for our DCNN as opposed to two steps in Webly to observe is the accuracy is within range of Webly and worthy of another refinement step.
The parameters of the CRF are mentioned in the appendix.\\
For saliency detection we use \cite{yzlrm13} 
and for CRF we use \cite{uk11}.\\

\textbf{Results:} We observe in Fig 3 that the result of saliency and CRF for foreground extraction is dissatisfactory. We note that the saliency is too strongly affected by colour. A bright
portion exhibits strong saliency to the extent that other parts of the foreground which are lighter in shade are almost totally ignored although a human would consider them 
a part of the same object. We additionally note that the presence of watermarks on the images, which is a common occurrence in online stock images from the web, heavily and poorly
affects the saliency phase leading to poor segmentation. Moreover an object wholly of light shade may totally be missed during foreground extraction. \\
Since saliency detection conceptually marks portions of an image which are most likely to capture a attention of a human, it may end up marking only a portion of the foreground
instead of the whole foreground leading to poor results overall. After 2 rounds of Webly prescribed CRF refinement iterations we only manage to achieve \textbf{32.08} mIoU.\\



\begin{figure}[t]
\begin{center}
\includegraphics[width=1\linewidth]{images/pascwhitebgpastely.png}
\end{center}
   \caption{Experiment: Pascal White Background: a) A PASCAL VOC training image b) Segmentation provided by PASCAL c) Using the pixel wise annotations provided by the training set we extract foreground and paste
   on white background.
   The segmentation map, along with image from b is fed to our DCNN for training. Realistic background images come from \textbf{C} }
\end{figure}

\subsection{Pure Pascal}

\textbf{Aim:} Trying to get best possible accuracy on our network with fully supervised training for purposes of comparison with subsequent experiments.\\

To establish some baselines (highest possible mIoU) for subsequent experiments we first train our DCNN end-to-end on PASCAL VOC 2012. In this fully supervised method we take the 1464 PASCAL VOC 2012
training set image pairs and test the trained network on 1449 images val set. The output is refined by 10 iterations of a CRF which
leads to an mIoU improvement of about 1.5. Parameters of the CRF are in the appendix.\\

\textbf{Results:} We achieve a mIoU of \textbf{69.01} which is the highest mIoU in all our experiments. As expected fully supervised training with perfectly labelled data produces
the best result.\\

\subsection{Pascal White Background}

\textbf{Aim:} Find out how much poor training data affects \textit{DCNN only Webly} experiment accuracy.\\

To test the significance of incorrect foreground extraction we move away from \textbf{W} images and instead go to PASCAL VOC 2012 training set. We take the already perfectly
segmented foreground regions and paste them on white background images. Since we paste the foregrounds we already know the pixel locations of the foreground and
generate a segmentation map image as well (Refer Fig. 4). We then take this set of images which acts as a replacement of the \textbf{W} set of images, and along with \textbf{C} 
images train our DCNN. The output is refined by 10 iterations of a CRF which leads to an mIoU improvement of about 1.5. Parameters of the CRF can be found in the appendix.\\

\textbf{Result:} We achieve \textbf{49.75} mIoU. Primary cause of failure seems to be labelling of white background pixels and common background pixels as the same class.
However perfect foreground segmented training data ensures significant improvement from \textit{DCNN only Webly} approach.\\


\begin{figure}[t]
\begin{center}
\includegraphics[width=1\linewidth]{images/pastely.png}
\end{center}
   \caption{Experiment: Pascal Pastely. Perfectly segmented foreground regions from PASCAL VOC training set are randomly pasted on common background images from \textbf{C} giving
   rise to synthetically generated augmented training data set}
\end{figure}


\subsection{Pascal Pastely}

\textbf{Aim:} See if \textit{Pastely} approach (described below) helps improve accuracy further.\\

At this point we feel that the idea of having separate \textbf{W} and \textbf{C} image sets may be challenged. We decide to take the perfectly
segmented foreground regions of PASCAL VOC training set and paste them randomly at random scales on common background images thus generating 14640 training images pairs,
10 times more than the previous experiment. This experiment makes two major changes:\\ 
1) We paste foregrounds on common backgrounds instead of white backgrounds\\
2) We copy the same foreground on multiple common background images to generate a large set of synthetic training data.\\
This synthetic generation of training data by pasting foreground on background images can be called \textit{Pastely}. 
Figure 5 explains \textit{Pastely} with illustrations. 
We take this augmented training set and train our DCNN. The output is refined with 10 iterations of a CRF which leads to an
mIoU improvement of about 1.5. Parameters of the CRF can be found in the appendix.\\

\textbf{Result:} We achieve \textbf{60.64} mIoU using this method. The drop in accuract compared to \textit{Pure Pascal} experiment seems to be due to \textit{domain shift}, as 
the common background images being used in this experiment do not accurately reflect the backgrounds in the PASCAL VOC challenge.



\begin{figure}[t]
\begin{center}
\includegraphics[width=1\linewidth]{images/transpastely.png}
\end{center}
   \caption{Experiment: Web Transparent Pastely. \textbf{a)} Transparent background image downloaded from the web. Although the background looks gray, in reality the image has
   an alpha channel where transparency is set to full. In effect pasting this image on another image will allow the gray portions to vanish and assume the base image's appearance.
   \textbf{b)} Foreground randomly pasted on common background images}
\end{figure}

\subsection{Web Transparent Pastely}

\textbf{Aim:} To try a new web based weakly supervised approach combining the learnings from the previous experiments.\\

The above experiments use perfectly segmented images and hence can not be called weakly supervised. Hence we decide to go back to the Webly approach in essence and
once again download images from the web, but this time we download images with transparent background instead of white background. Raster file formats that support 
transparency include GIF, PNG, BMP, TIFF, and JPEG 2000, through either a transparent color or an alpha channel (Refer Figure 6). Most vector formats 
implicitly support transparency because they simply avoid putting any objects at a given point. This includes EPS and WMF. 
A decent number of images were available online to experiment with. We took these perfectly segmented foreground images 
found online and randomly pasted them at random scales on common background images. We use no human supervision at all and hence we also have images in the training 
set which belong to the wrong class.
We take this augmented training set and train our DCNN. The output is refined with 10 iterations of a CRF which leads to an mIoU
improvement of about 1.5. Parameters of the CRF can be found in the appendix.
\\

\textbf{Results:} We achieve an mIoU of \textbf{56.11}. This outperforms Webly while requiring a fraction of training data and never using any image from the PASCAL VOC dataset. We also
use no manual annotation during the process.

\begin{figure*}[t]
\begin{center}
\includegraphics[width=1\linewidth]{images/combsegs.png}
\end{center}
   \caption{ Qualitative results on the PASCAL VOC 2012 validation set of our experiments along with ground truth}
\end{figure*}



\section{Results and Discussion}

\begin{center}
\begin{tabular}{ | m{10em} | m{1cm}|} 
\hline
Method & mIoU \\ 
\hline
Webly \cite{7532767} &  \textbf{53.40} \\ 
\hline
DCNN Only Webly & 32.08 \\ 
\hline
Pure Pascal & 69.01 \\
\hline
Pascal White BG & 49.75 \\
\hline
Pascal Pastely & 60.64 \\
\hline
Web Transparent Pastely & \textbf{56.11} \\
\hline
\end{tabular}
\end{center}


We make the following observations:

\begin{itemize}
  \item Foreground extraction from white background images suffers due to limitations of saliency detection leading to generation of poor quality of training data
  \item In \textit{DCNN Only Webly} experiment the neural network struggles to distinguish white background from common background as we mark both as the same background class
  \item Providing perfect segmentations on realistic backgrounds for training in experiment \textit{Pure Pascal} achieves best results. This is a fully supervised method.
  \item Using the same Pascal foregrounds on common but unrealistic backgrounds lowers accuracy due to domain shift
  \item Pasting the same foreground in different locations on the same background image produces useful augmented training data
  \item Using only 50 transparent background images per class including noise (images which belong to the wrong class) combined with Pastely outperforms Webly.
  \item A combined effect of using perfect segmentation as input and augmented synthetic training data help outperform Webly with a fraction of the input data requirement
  and a much simpler training procedure (1000 images vs 6807 images)
\end{itemize}

Since we do not use perfect human annotations as training data we call both Webly and Web-Transparent-Pastely weakly supervised approaches. However it must be 
noted that creating white background or transparent background images also requires a similar amount of effort when compared to perfect human annotations for semantic segmentation.
However since these images were not created with image segmentation training in mind and they are freely available on the web we take the liberty of claiming that
we have avoided human annotation effort. Moreover being reliant on a web search engine has its own pitfalls. The search engine may stop serving queries of a certain type,
an example of which would be the Google Image API which has recently been deprecated.\\
Future work in this area could focus on creating more realistic synthetic images using Generative Adversarial Networks. One could also explore using web videos for training data
generation as background subtraction in videos is often a simpler task due to moving foreground and stationary background.

\section{Acknowledgment}
This report was prepared as a requirement for the completion of course MA-INF 2307 of University of Bonn. The first author of this report was guided by the second and third 
authors. Without the extra-ordinary effort of the guides the successful completion of this report would not have been possible.

\section{Appendix}

The CRF used for refinement of our DeepLab-LargeFOV \cite{2016arXiv160600915C} network is a part of the DeepLab framework and is implemented as a layer. In our networks the CRF is the last layer.
The following parameters were used for the CRF:\\
max\_iter\: 10\\
pos\_w\: 3\\                   
pos\_xy\_std\: 3\\
bi\_w\: 5\\
bi\_xy\_std\: 50\\
bi\_rgb\_std\: 10\\

The implementation of this report is hosted online at \href{https://gitlab.com/debayan/weblycvlab}{https://gitlab.com/debayan/weblycvlab}.

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\end{document}
