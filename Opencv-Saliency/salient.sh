#!/bin/bash

FOLDER="../images/resizedCleanWhiteBg101/"

#resize to either height or width, keeps proportions using imagemagick
images="$(find $FOLDER -iname '*.jpg' -o -iname '*.png')"
for image in $images; do
    echo $image
    ./SaliencyDetection "$image"
done
